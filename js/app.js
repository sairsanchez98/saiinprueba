// When the user scrolls down 80px from the top of the document, chage the navbar color and more.
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
    document.getElementById("navbar").style.background = "#092854";
    document.getElementById("icon-small").style.color = "#fff";
    document.getElementById("icon-small0").style.color = "#fff";
    document.getElementById("icon-small1").style.color = "#fff";
    document.getElementById("icon-small2").style.color = "#fff";
    document.getElementById("icon-small3").style.color = "#fff";
   
    
  } else {
    document.getElementById("navbar").style.background = "transparent";
    document.getElementById("icon-small").style.color = "#092854";
    document.getElementById("icon-small0").style.color = "#092854";
    document.getElementById("icon-small1").style.color = "#092854";
    document.getElementById("icon-small2").style.color = "#092854";
    document.getElementById("icon-small3").style.color = "#092854";
    
  }
}

//modal teacher
const modal_register_teacher = document.getElementById('modal-register-teacher');
const open_modal_teacher = document.getElementById('open-modal-teacher');
const close_teacher = document.getElementById('close-teacher');
open_modal_teacher.addEventListener('click', () => {
modal_register_teacher.setAttribute('open', 'true')
});
close_teacher.addEventListener('click', () => {
modal_register_teacher.removeAttribute('open')
});

//modal edit teachear
const modal_edit_teacher = document.getElementById('modal-edit-teacher');
const open_modal_edit_teacher = document.getElementById('open-edit-teacher');
const close_edit_teacher = document.getElementById('close-teacher-edit');
open_modal_edit_teacher.addEventListener('click', () => {
modal_edit_teacher.setAttribute('open', 'true')
});
close_edit_teacher.addEventListener('click', () => {
modal_edit_teacher.removeAttribute('open')
});

//delete teacher
const modal_delete_teacher = document.getElementById('modal-delete-teacher');
const open_delete_teacher = document.getElementById('open-delete-teacher');
const close_teacher_delete = document.getElementById('close-teacher-delete');
open_delete_teacher.addEventListener('click', () => {
modal_delete_teacher.setAttribute('open', 'true')
});
close_teacher_delete.addEventListener('click', () => {
modal_delete_teacher.removeAttribute('open')
});





