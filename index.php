<?php 
session_start();
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="theme-color" content="#092854">
    <link rel="stylesheet" href="css/main.css">
    <title>SAIIN page</title>

</head>

<body>
   <?php 
        if (!isset($_SESSION["logged"])) {
            ?>
             <div id="StartApp">
                <!-- ************* START MODAL LOGIN ******-->
                <login></login>
                <!-- *************END MODAL LOGIN ******-->

                <!-- START Modal buttons REGISTER-->
                <registro></registro>
                <!-- END modal buttons REGISTER-->
                
                <vheader></vheader>

                <info></info>
                
                <router-view></router-view>

                <vfooter><vfooter>
            </div>
            <?php
        }
    ?>

    <script src="js/app.js"></script><!-- modals and navbar scroll -->
    <script src="js/all.js"></script> <!-- Fontawesome icons -->   
    <script src="Assets/Vue.js"></script> <!-- Vue js -->
    <script src="Assets/Axios.js"></script>
    <script src="Assets/Router.js"></script>



    <!-- COMPONENTES -->
    <script src="Componentes/dinamyc/js/login.js"></script>
    <script src="Componentes/dinamyc/js/registro.js"></script>
    <script src="Componentes/static/js/header.js"></script>
    <script src="Componentes/dinamyc/js/info.js"></script>
    <script src="Componentes/static/js/footer.js"></script>
    <!-- COMPONENTES -->

    <script src="Controladores/js/ctrl.main.js"></script> <!-- controlador inicial js -->
</body>




</html>